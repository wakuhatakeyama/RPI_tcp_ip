import socket
import time
from contextlib import closing

from ctypes import *

def recv():
  host = '192.168.1.7'
  port = 8080
  backlog = 10
  bufsize = 4096
  socket.timeout(1)

  sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
  try:
    with closing(sock):
      sock.bind((host, port))
      sock.listen(backlog)
      conn, address = sock.accept() 
      msg = conn.recv(bufsize)
      print(msg)
      if (msg == b'raspberry'):
        print("Hello raspberry!")

  except socket.timeout:
    print(b'socketTimeout')
  except socket.error:
    print(b'socketError')

def popup():
  user32 = windll.user32
  user32.MessageBoxA( 0, "Enemy Will Come Here!!", "Caution!!", 0x00000030)

if __name__ == '__main__':
    try:
        while True:
            recv()
            time.sleep(1)
    except KeyboardInterrupt:
        print("key interrupt occuered")
